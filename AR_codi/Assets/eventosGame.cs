using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class eventosGame : MonoBehaviour
{
    public GameObject menu,menuobj,victoria;
    public TextMeshPro[] numbers;
    public GameObject[] cajas;
   
    public int C_manzanas,C_plantanos;
    public int L_manzanas, L_platanos;
    public bool manzanasllenas,platanosllenos;
    int objectivos=0;
    // Start is called before the first frame update
    void Start()
    {
      //  manzanas = 0;
     //   plantanos = 0;
        menu.SetActive(false);
        menuobj.SetActive(false);
        platanosllenos = false;
        manzanasllenas = false;
    }
    public void nextlevel(bool cajas) {
        if (cajas)
        {
            objectivos++;
            if (objectivos>=2)
            {
                victoria.SetActive(true);
               // Invoke("cargalevel",5);
                
            }
        }
    }
   public void cargalevel() {
        SceneManager.LoadScene("leccion1");
    }
    public void counter(int frutas) {
        if (frutas==1)
        {
            C_manzanas++;
            numbers[0].text =" "+ C_manzanas;
            cajas[0].GetComponent<Animator>().Play("big");
            if (C_manzanas>= L_manzanas)
            {
                manzanasllenas = true;
                nextlevel(manzanasllenas);
            }
        }
        if (frutas==2)
        {
            C_plantanos++;
            numbers[1].text = " "+ C_plantanos;
            cajas[2].GetComponent<Animator>().Play("big");
            if (C_plantanos >= L_platanos)
            {
                platanosllenos = true;
                nextlevel(platanosllenos);
            }
        }
        if (frutas == 3)
        {
          //  plantanos++;
          //  numbers.text = " " + plantanos;
            cajas[3].GetComponent<Animation>().Play();
        }
        if (frutas == 4)
        {
          //  plantanos++;
          //  numbers.text = " " + plantanos;
            cajas[4].GetComponent<Animation>().enabled = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
