using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class conect_2 : MonoBehaviour
{
    public GameObject[] luces;
    public Collider[] areas;
    public GameObject area,particulas;
    public bool plug=false;
    public Animator anim;
    
    // Start is called before the first frame update
    void Start()
    {
        
        foreach (var item in luces)
        {
            item.SetActive(false);
        }
        areas = area.GetComponentsInChildren<Collider>(); 
        foreach (Collider child in areas)
        {
            child.GetComponent<Collider>().enabled = false;
        }
       
    }
    public void prende() {

        if (plug)
        {
            foreach (Collider child in areas)
            {
                child.GetComponent<Collider>().enabled = true;
            }
            anim.SetTrigger("on");
          
        }
        
    }
    private void OnMouseDown()
    {
        foreach (var item in luces)
        {
            item.SetActive(true);
            
        }
        var cubeRenderer = GetComponent<Renderer>();
        cubeRenderer.materials[1].SetColor("_Color", Color.green);
        particulas.SetActive(false);
        plug = true;
          
           
       
       

    }
}
