using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class managergame2 : MonoBehaviour
{
    public GameObject menunext,intro, agente;
 
    // Start is called before the first frame update
    void Start()
    {
        Invoke("introact",4);
    }
    public void menu() {

        menunext.SetActive(true);
            }
    public void gamenext() {
        Invoke("nextlevel",1);
    }
    public void introact() {
        intro.SetActive(false);
        agente.SetActive(true);
    }
    void nextlevel()
    {
        SceneManager.LoadScene("leccion2");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
