using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
    [RequireComponent(typeof(NavMeshAgent))]
public class raycast_2 : MonoBehaviour
{
    public Animator anim;
    public managergame2 manager;
    private NavMeshAgent myagent;
   // RaycastHit hit;
    public GameObject pointer;
    public Camera cameraM;
    public  float speed, rotspeed;
    bool moving;
    Vector3 lookAttarget,targetposs;
    Quaternion playeRot;
    // Start is called before the first frame update
    void Start()
    {
        myagent = GetComponent<NavMeshAgent>();
    }
    private void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clicktomove();
        }
        Debug.Log(Vector3.Distance(myagent.destination, transform.position )+ " "+ myagent.stoppingDistance);
        if (Vector3.Distance(myagent.destination,transform.position)<= myagent.stoppingDistance)
        {
           // Debug.Break();
            anim.SetBool("cami", false);
        }

    }
 
    void clicktomove() {
        Ray ray = cameraM.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        bool hashit = Physics.Raycast(ray,out hit);
        if (hashit)
        {
            myagent.isStopped = false;
            setDestination1(hit.point);
        }
      
    }
    private void setDestination1(Vector3 target)
    {

        myagent.SetDestination(target);
        pointer.transform.position = target;
        anim.SetBool("cami",true);
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("portal"))
        {
            manager.menu();
        }
       
    }
}
