using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
public class leccionmanager : MonoBehaviour
{
    [SerializeField]
    GameObject manzana,menu;
    [SerializeField]
    TextMeshPro texto;
    public string textodialogo;
    public int tiempo;
    public string nivel;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("leccion1_C");
    }
    IEnumerator leccion1_C()
    {

        yield return new WaitForSeconds(tiempo);
        texto.text =textodialogo;
        manzana.SetActive(true);
        yield return new WaitForSeconds(tiempo);
        menu.SetActive(true);
    }
    public void nextlevel() {
        SceneManager.LoadScene(nivel);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
