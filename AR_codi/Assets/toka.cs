using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using PathCreation.Examples;
public class toka : MonoBehaviour
{
    int contador;
    BoxCollider mibox;
    public GameObject victoria,nubes;
 
    public Animator anim;
    public PathFollower path;
    // Start is called before the first frame update
    void Start()
    {
        mibox = GetComponent<BoxCollider>();
        contador = 0;
        anim=GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnMouseDown()
    {
        path.enabled = true;
       
    }
    public void desactivate() {
        nubes.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("salto"))
        {
            other.GetComponent<spawn>().habilitando();
            anim.enabled = true;
            Invoke("desactivate",2);
        }
    }
}
